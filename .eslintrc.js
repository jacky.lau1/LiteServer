module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "indent": [
            "error",
            4,
            { "SwitchCase": 1 }
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    },
    "globals": {
        "process": true,
        "describe": true,
        "before": true,
        "after": true,
        "it": true,
        "__dirname": true,
    },
    "overrides": [
        {
            "files": ["**/*.js"],
            "rules": {
                "no-unused-vars": "off",
                "no-console": "off"
            }
        }
    ]
};