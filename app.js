require('colors');
const Koa = require('koa'),
    fs = require('fs'),
    Controller = require('./controller');

/**
 * A Simple Server inheriting KOA. You can further modify it by overriding its methods
 * @type {module.LiteServer}
 */

module.exports = class LiteServer extends Koa {
    constructor(options, middlewares) {
        super();
        this._initSubwares(options, middlewares);
        this._initController(options);
    }

    _initSubwares(options = {}, middlewares = [
        'compress',
        'cors',
        'body',
        'logger',
        'helmet',
        'limit',
        'session',
        'static',
        'defaultOutput',
        'errorHandler',
        'oauth'
    ]) {

        middlewares.forEach(file => {
            this.use(require(__dirname + `/src/${file}`)(options, this));
            return file;
        });

        console.info('Middlewares Imported'.cyan);
    }

    /**
     * Override to insert your own middlewares
     * @param options
     * @protected
     */
    _initController(options = {}) {
        let router = Controller(options);
        this.use(router);
        console.info('Controller Imported'.cyan);
    }

    showRouteRule() {
        return Controller();
    }

    listen(port) {
        console.info(`Server Listening At ${port} ...`.cyan);
        return super.listen(port);
    }
};
