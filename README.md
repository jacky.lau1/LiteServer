# LiteServer
> KOA Server Wrapper, with simple routing functions

### Installation
Install using npm:
```sh
npm install git+http://gitlab.bibsolution.net/jacky.lau/LiteServer
```

### Example

folder structure:
```
├── app.js
├── controller
        └── user
              └── select.js
```
./app.js
```js
let LiteServer = require('LiteServer');
let app = new LiteServer(options); // refer to Options
app.listen(3000);
```
./controller/user/select.js (`routing modules`, which have the methods with name `post` or `get`)
```js
module.exports = class {
    async post() {
        return {
            msg: 'Post...',
            params: this.params, // obtain body params
            files: this.files, // get multipart files
        };
    }
    async get() {
        return {
            msg: 'Get...',
            params: this.params, // obtain query string
        };
    }
}
```
Call http request
```sh
curl "http://localhost:3000"   
#404
curl "http://localhost:3000/user/select?a=1&b=2"   
#{"success":true,"data":{"msg":"Get...","params":{"a":1,"b":2},"files":[]}}
curl "http://localhost:3000/user/select" -d "a=3&b=4"   
#{"success":true,"data":{"msg":"Post...","params":{"a":3,"b":4} }}
```

### Url Params
use `_{filename}` represent `:{param}` as url.
```
├── app.js
├── controller
        └── user
              └── _id
                    └── _name.js
```
**_name.js:**
```js
module.exports = class {
    async get(){
        return this.params
    }
}
```
**curl:**
```sh
curl "http://localhost:3000/user/10/jacky?a=1"
#{"success":true,"data":{"id":10,"name":"jacky","a":1}}
```


### Default Middlewares
- koa-logger
- koa-helmet
- koa-better-ratelimit
- koa-session with json db store
- koa-body
- controller-router extending koa-router
- oauth2-server

### Options
```js
new LiteServer({

    controller: {
        disabled, // default false
        root,  
        // default './controller', the folder of routing modules
        methods,
        // default ["get", "post"], load methods from routing modules
        invalidPageError // default false
    },
    
    defaultoutput: {
        disabled, // default false
        customOutput
        // async (ctx, next) => { ctx.body = output; }
    },
    
    body: {
        disabled // default false
        // see [https://github.com/dlau/koa-body]
    },
    
    helmet: {
        disabled // default false
        // see [https://github.com/venables/koa-helmet]
    },
    
    limit: {
          disabled // default false
          // see [https://www.npmjs.com/package/koa2-ratelimit]
    },
    
    logger: {
        disabled // default false
        // see [https://github.com/koajs/logger]
    },
    
    session: {
        disabled // default false
        // see [https://github.com/koajs/session]

        // You can override the store like this, default store is using lowDB
        store: {
            get: key => {},
            set: (key, sess, maxAage, { changed }) => {},
            destroy: key => {}
        }
    },

    cors: {
        disabled // default false
        options: {
            origin,
            exposeHeaders,
            maxAge,
            credentials,
            allowMethods,
            allowHeaders
        }
        // see [https://www.npmjs.com/package/koa2-cors]
    },

    oauth: {
        disabled, // default true
        at: "/token",
        except: ["/"],
        models: {
            // you can override the model functions here
            // default is using lowDB
        }

        // see [https://oauth2-server.readthedocs.io/en/latest/]
    },
    
    onerror
    // default e => e. Error Handler
    
})
```

### OAuth
you can turn on the oauth like this:
```js
new LiteServer({
    oauth: {
        disabled: false
    }
});
```
once you run the server, ``client.json``, ``user.json``, ``oauth.json`` will be created at directory root.

You need to insert client and user object by your own program, with format:

**client.json**
```json
{
    "data": [
        {
            "id": 1,
            "secret": "A secret string",
            "grants": ["password", "refresh_token"],
            "redirectUris": []
        }
    ]
}
```
**user.json**
```json
{
    "data": [
        {
            "id": 1,
            "username": "tester",
            "password": "A password hash"
        }
    ]
}
```
It is highly recommended that, you should write your user checking function, with your user db.
```js
new LiteServer({
    oauth: {
        disabled: false,
        model: {
            getUser: (username, password, callback) => {
                // the user object must contain id.
                callback({ id: 204 });
            }
        }
    }
});
```