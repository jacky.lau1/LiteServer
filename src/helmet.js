const Helmet = require('koa-helmet');

module.exports = ({ helmet = { disabled: false } }) => {
    if (helmet.disabled) return async (ctx, next) => await next();
    return Helmet(helmet);
};
