module.exports = ({ onerror = e => e }) => {
    return async (ctx, next) => {
        try {
            return await next();
        } catch (e) {
            await onerror(e);
            throw e;
        }
    };
};
