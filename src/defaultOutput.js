module.exports = ({ defaultoutput = { disabled: false, customOutput: undefined } }) => {
    if (defaultoutput.disabled) {
        if (defaultoutput.customOutput && typeof defaultoutput.customOutput === 'function') {
            return defaultoutput.customOutput;
        } else {
            return async (ctx, next) => {
                try {
                    ctx.body = await next();
                } catch (err) {
                    ctx.body = err;
                }
            };
        }
    }
    return async (ctx, next) => {
        try {
            ctx.body = JSON.stringify({
                success: true,
                data: await next()
            });
        } catch (error) {
            ctx.body = JSON.stringify({
                success: false,
                error: error instanceof Error ? error.message : error
            });
        }
    };
};
