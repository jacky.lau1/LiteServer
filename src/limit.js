const Limiter = require('koa2-ratelimit').RateLimit;

module.exports = ({
    limit = { disabled: false, interval: 60000, max: 20, message: 'Let\'s take a rest.' }
}) => {
    if (limit.disabled) return async (ctx, next) => await next();
    return Limiter.middleware(limit);
};
