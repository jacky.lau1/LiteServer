const koaStatic = require('koa-static');
module.exports = ({ setting = { disabled: true, root: undefined, options: {} } }) => {
    if (setting.disabled && !setting.root) return async (ctx, next) => await next();
    return koaStatic(setting.root, setting.options);
};