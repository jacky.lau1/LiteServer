const Compress = require('koa-compress');
module.exports = ({ compress = { disabled: true } }) => {
    if (compress.disabled) return async (ctx, next) => await next();
    return Compress({
        filter: (content_type) => {
            return /text/i.test(content_type);
        },
        threshold: 2048,
        flush: require('zlib').Z_SYNC_FLUSH
    });
};