const Logger = require('koa-logger');

module.exports = ({ logger = { disabled: false } }) => {
    if (logger.disabled) return async (ctx, next) => await next();
    return Logger(logger);
};
