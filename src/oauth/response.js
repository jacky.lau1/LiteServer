const OauthServer = require('oauth2-server'),
    Res = OauthServer.Response;

module.exports = ctx => {
    let { response } = ctx;
    let { header } = response;
    return new Res({
        ...response,
        headers: { ...header }
    });
};
