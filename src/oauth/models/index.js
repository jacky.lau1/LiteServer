module.exports = {
    getAccessToken: require('./oauth_getAccessToken'),
    getRefreshToken: require('./oauth_getRefreshToken'),
    getClient: require('./oauth_getClient'),
    saveToken: require('./oauth_saveToken'),
    getUser: require('./oauth_getUser'),
    revokeToken: require('./oauth_revokeToken')
};
