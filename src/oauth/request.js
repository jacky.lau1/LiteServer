const OauthServer = require('oauth2-server'),
    Req = OauthServer.Request;

module.exports = ctx => {
    let { request } = ctx;
    let { method, url, header } = request;
    return new Req({
        headers: header,
        method,
        query: request.query || {},
        body: request.body || {}
    });
};
