const chai = require('chai'),
    expect = chai.expect,
    LiteServer = require('../app');
const app = new LiteServer({
        controller: { root: './test/controller' }
    }),
    rules = app.showRouteRule();
let server;

chai.use(require('chai-http'));

describe('status test (expect 200)', function () {
    before(() => {
        server = app.listen(3001);
    });
    after(() => {
        server.close();
    });

    rules.forEach(({ method, location }) => {
        it(`${method} to ${location}`, done => {
            chai.request('http://localhost:3001')[method](location)
                .send({})
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });
});

describe('post to /', function () {
    before(() => {
        server = app.listen(3001);
    });
    after(() => {
        server.close();
    });
    it('should have return params', done => {
        let data = { a: 1, b: 2 };

        chai.request('http://localhost:3001')
            .post('/')
            .send(data)
            .end((err, res) => {
                let { success, data } = JSON.parse(res.text);
                expect(success).to.be.true;
                expect(data).to.deep.equal(data);
                done();
            });
    });
});

describe('get to /dir/throw_error_test', function () {
    before(() => {
        server = app.listen(3001);
    });
    after(() => {
        server.close();
    });
    it('should have return error', done => {
        chai.request('http://localhost:3001')
            .get('/dir/throw_error_test')
            .end((err, res) => {
                let { success, error } = JSON.parse(res.text);
                expect(success).to.be.false;
                expect(error).to.equal('get');
                done();
            });
    });
});
