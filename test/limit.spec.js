const chai = require('chai'),
    expect = chai.expect,
    LiteServer = require('../app');
const app = new LiteServer({
    controller: { root: './test/controller' },
    limit: { interval: 60000, max: 1 }
});
let server;

chai.use(require('chai-http'));

describe('post to /', function() {
    before(() => {
        server = app.listen(3001);
    });
    after(() => {
        server.close();
    });
    it('first request should pass', done => {
        chai.request('http://localhost:3001')
            .post('/')
            .end((err, res) => {
                expect(res.status).to.equal(200);
                done();
            });
    });
    it('second request should fail', done => {
        chai.request('http://localhost:3001')
            .post('/')
            .end((err, res) => {
                expect(res.status).to.equal(429);
                done();
            });
    });
});
