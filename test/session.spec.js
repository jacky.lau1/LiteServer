const chai = require('chai'),
    request = require('supertest'),
    LiteServer = require('../app');
const app = new LiteServer({
    controller: { root: './test/controller' }
});
let server;

chai.use(require('chai-http'));

describe('post to /', function() {
    before(() => {
        server = app.listen(3001);
    });
    after(() => {
        server.close();
    });
    it('set session', done => {
        request(server)
            .post('/dir/session_test')
            .expect('Set-Cookie', /lite:sess/)
            .expect(200, (err, res) => {
                done();
            });
    });
    it('get session', done => {
        request(server)
            .get('/dir/session_test')
            .expect(200, (err, res) => {
                done();
            });
    });
});
