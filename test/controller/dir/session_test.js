module.exports = class {
    constructor() {}

    async post() {
        this.ctx.session.id = Math.round(Math.random() * 10000);
        this.ctx.session.value = Math.round(Math.random() * 10000);
        return this.ctx.session;
    }

    async get() {
        return this.ctx.session;
    }
    
    async delete(){
        this.ctx.session = null;
        return this.ctx.session;
    }
};
