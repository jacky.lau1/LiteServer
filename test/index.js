const LiteServer = require('../app');
const app = new LiteServer({
    controller: { root: './test/controller', methods: ['get', 'post', 'delete'], invalidPageError:true },
    oauth: { disabled: false, except: ['/', '/dir/throw_error_test'] },
    onerror: err => {
        console.error(err);
    }
});
app.listen(3001);
