const chai = require('chai'),
    expect = chai.expect,
    LiteServer = require('../app');
const app = new LiteServer({
    controller: { root: './test/controller' },
    helmet: {}
});
let server;

chai.use(require('chai-http'));

describe('post to /', function() {
    before(() => {
        server = app.listen(3001);
    });
    after(() => {
        server.close();
    });
    it('check headers', done => {
        chai.request('http://localhost:3001')
            .post('/')
            .end((err, res) => {
                expect(res).to.have.header('x-frame-options');
                expect(res).to.have.header('x-xss-protection');
                expect(res).to.have.header('strict-transport-security');
                expect(res).to.have.header('x-content-type-options');
                done();
            });
    });
});
